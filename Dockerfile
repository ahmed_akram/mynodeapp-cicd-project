FROM node:16-alpine

WORKDIR /usr/src/app

COPY ./app/package*.json ./
RUN npm ci
COPY ./app .

EXPOSE 3000
CMD ["npm", "start"]
